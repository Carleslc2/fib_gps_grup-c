package test.integration.org.testinfected.petstore.jdbc;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.OrderNumberDatabaseSequence;
import org.testinfected.petstore.order.OrderNumber;

import test.support.org.testinfected.petstore.jdbc.Database;

public class OrderNumberDatabaseSequenceTest {

	private Database database;
	private Connection connection;
	private OrderNumberDatabaseSequence orderNumberDBSequence;
	
	@Before
	public void setUp() throws Exception {
		database = Database.test();
		connection = database.connect();
		orderNumberDBSequence = new OrderNumberDatabaseSequence(connection);
	}

	@After
	public void tearDown() throws Exception {
		database.clean();
		connection.close();
		database = null;
		connection = null;
		orderNumberDBSequence = null;
	}

	@Test
	public void testAutoincrement() {
		OrderNumber orderNumber = orderNumberDBSequence.nextOrderNumber();
		Integer i = Integer.parseInt(orderNumber.getNumber());
		orderNumber = orderNumberDBSequence.nextOrderNumber();
		assertEquals(i+1, Integer.parseInt(orderNumber.getNumber()));
	}

}
