package test.unit.org.testinfected.petstore.controllers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.fail;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.controllers.ListProducts;
import org.testinfected.petstore.product.AttachmentStorage;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.product.ProductCatalog;

import com.vtence.molecule.Request;
import com.vtence.molecule.Response;

import test.support.org.testinfected.petstore.builders.ProductBuilder;

public class ListProductsTest {

	private ProductCatalog productCatalog;
	private FakeViewProducts fakeView;
	private AttachmentStorage attachmentStorage;
	private ListProducts listProducts;
	private String productNumber;


	@Before
	public void setUp() throws Exception {
		productCatalog = new FakeProductCatalog();
		Product product = ProductBuilder.aProduct().build();
		productNumber = product.getNumber();
		productCatalog.add(product);
		fakeView = new FakeViewProducts();
		attachmentStorage = new DummyAttachmentStorage();
		listProducts = new ListProducts(productCatalog, attachmentStorage, fakeView);
	}

	@After
	public void tearDown() throws Exception {
		productCatalog = null;
		fakeView = null;
		attachmentStorage = null;
		listProducts = null;
	}

	@Test
	public void testListProductsByKeyword()  {
		try {
			listProducts
			.handle(new Request()
					.addParameter("keyword", productNumber), new Response());
			assertThat("available products", fakeView.getProducts(), everyItem(hasProductNumber(productNumber)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	private Matcher<Product> hasProductNumber(final String number) {
		return new FeatureMatcher<Product, String>(equalTo(number), "has product number","product number") {
			protected String featureValueOf(Product actual) {
				return actual.getNumber();
			}
		};
	}



}
