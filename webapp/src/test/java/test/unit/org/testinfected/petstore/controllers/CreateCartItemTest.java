package test.unit.org.testinfected.petstore.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.testinfected.petstore.controllers.CreateCartItem;
import org.testinfected.petstore.order.Cart;
import org.testinfected.petstore.order.CartItem;

import com.vtence.molecule.Request;
import com.vtence.molecule.Response;
import com.vtence.molecule.session.Session;


public class CreateCartItemTest {
	
	private String itemNumber = "01031996";
	
	@Test
	public void addItemToCartBeforeRedirecting() throws Exception {
		Request req = new Request();
		new Session().bind(req);
		
		req.addParameter("item-number", itemNumber);
		Response res = new Response();
		
		CreateCartItem cci = new CreateCartItem(new ItemIntenvoryFake());
		cci.handle(req, res);
		
		List<CartItem> list = ((Cart)Session.get(req).get(Cart.class)).getItems();
		
		boolean found = false;
		for (CartItem ci : list ) {
			if (ci.getItemNumber().equals(itemNumber)) {
				found = true;
				break;
			}
		}
		
		assertThat(found, is(true));
		assertThat(res.redirectTo("/cart").isDone(), is(true));
	}
}
