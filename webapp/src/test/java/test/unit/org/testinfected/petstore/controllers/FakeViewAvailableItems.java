package test.unit.org.testinfected.petstore.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testinfected.petstore.View;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.views.AvailableItems;

import com.vtence.molecule.Response;

/**
 * @author Carlos Lázaro Costa (carlos.lazaro.costa@est.fib.upc.edu)
 */
public class FakeViewAvailableItems implements View<AvailableItems> {

	private AvailableItems items;
	
	@Override
	public void render(Response response, AvailableItems context) throws IOException {
		items = context;
	}

	public List<Item> getAvailableItems() {
		List<Item> itemsHandled = new ArrayList<Item>();
		items.getEach().forEach(itemsHandled::add);
		return itemsHandled;
	}

}
