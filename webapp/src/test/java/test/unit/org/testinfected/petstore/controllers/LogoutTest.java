package test.unit.org.testinfected.petstore.controllers;


import org.junit.Test;
import org.testinfected.petstore.controllers.Logout;
import org.testinfected.petstore.product.Item;

import com.vtence.molecule.session.Session;
import com.vtence.molecule.Request;
import com.vtence.molecule.Response;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;


public class LogoutTest {

	@Test
	public void SessionDestruction(){
		Request r = new Request();
		Session s = new Session();
		s.bind(r);
		Logout l = new Logout();
		l.handle(r,new Response());
		assertThat("Session invalidated",Session.get(r).invalid());
	}
	

}
