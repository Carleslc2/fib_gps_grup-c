package test.unit.org.testinfected.petstore.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testinfected.petstore.View;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.views.Products;

import com.vtence.molecule.Response;

/**
 * @author David Segovia Tomas (david.segovia@est.fib.upc.edu)
 */
public class FakeViewProducts implements View<Products> {

	private Products products;
	
	@Override
	public void render(Response response, Products context) throws IOException {
		// TODO Auto-generated method stub
		products = context;
	}
	
	public List<Product> getProducts() {
		List<Product> result = new ArrayList<>();
		products.getEach().forEach(result::add);
		return result;
	}

}
