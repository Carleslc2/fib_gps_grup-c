package test.unit.org.testinfected.petstore.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testinfected.petstore.product.DuplicateProductException;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.product.ProductCatalog;

/**
 * @author David Segovia Tomas (david.segovia@est.fib.upc.edu)
 */
public class FakeProductCatalog implements ProductCatalog{
	
	private Map<String, Product> catalog;
	
	public FakeProductCatalog() {
		catalog = new HashMap<>();
	}
	
	public void add(Product product) throws DuplicateProductException {
		catalog.put(product.getNumber(), product);
	}

    public Product findByNumber(String productNumber) {
    	return catalog.get(productNumber);
    }

	public List<Product> findByKeyword(String keyword) {
		List<Product> res = new ArrayList<>();
		for (Map.Entry<String, Product> entry : catalog.entrySet()) {
			Product p = entry.getValue();
			String descr = p.getDescription();
			String number = p.getNumber();
			String name = p.getName();
			if (descr != null) {
				if (descr.contains(keyword)) {
					res.add(p);
				}
			}
			else if (number != null) {
				if (number.contains(keyword))
					res.add(p);
			}
			else if (name != null) {
				if (name.contains(keyword))
					res.add(p);
			}
		}
		return res;
	}

}
